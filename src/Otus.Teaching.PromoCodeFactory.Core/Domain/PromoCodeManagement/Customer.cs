﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer :BaseEntity
    {
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }
        
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [Required]
        [MaxLength(255)]
        public string Email { get; set; }

        public ICollection<CustomerPreference> CustomerPreferences { get; set; }

        public ICollection<PromoCode> PromoCodes { get; set; }

    }
}