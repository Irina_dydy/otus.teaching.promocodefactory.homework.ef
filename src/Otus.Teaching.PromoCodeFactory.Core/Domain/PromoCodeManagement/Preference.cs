﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference :BaseEntity
    {
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        public ICollection<CustomerPreference> CustomerPreferences { get; set; }
    }
}