﻿using System;
using System.ComponentModel.DataAnnotations;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode : BaseEntity
    {
        [Required]
        [MaxLength(255)]
        public string Code { get; set; }

        [Required]
        [MaxLength(1000)]
        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        [Required]
        [MaxLength(200)]
        public string PartnerName { get; set; }

        public Guid? PartnerManagerId { get; set; }

        public Employee PartnerManager { get; set; }

        public Guid PreferenceId { get; set; }

        public Preference Preference { get; set; }

        public Guid CustomerId { get; set; }

        public Customer Customer { get; set; }
    }
}