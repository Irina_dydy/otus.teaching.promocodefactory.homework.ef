﻿using System.Linq;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Infrastructure.Profiles
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            // DTOs

            // Role controller
            CreateMap<Role, RoleItemResponse>();

            // Customer
            CreateMap<Customer, CustomerShortResponse>();
            CreateMap<Customer, CustomerResponse>()
                .ForMember(dest => dest.Preferences,
                    opt => opt.MapFrom(src => src.CustomerPreferences.Select(x => x.Preference)));
            CreateMap<CreateOrEditCustomerRequest, Customer>()
                .ForMember(dest => dest.Id,
                    opt => opt.Ignore())
                .ForMember(dest => dest.PromoCodes,
                    opt => opt.Ignore())
                .ForMember(dest => dest.CustomerPreferences,
                    opt => opt.Ignore());

            // PromoCodes
            CreateMap<PromoCode, PromoCodeShortResponse>();
            CreateMap<GivePromoCodeRequest, PromoCode>()
                .ForMember(dest => dest.Code,
                    opt => opt.MapFrom(src => src.PromoCode))
                .ForMember(dest => dest.BeginDate,
                    opt => opt.Ignore())
                .ForMember(dest => dest.EndDate,
                    opt => opt.Ignore())
                .ForMember(dest => dest.Customer,
                    opt => opt.Ignore())
                .ForMember(dest => dest.CustomerId,
                    opt => opt.Ignore())
                .ForMember(dest => dest.PreferenceId,
                    opt => opt.Ignore())
                .ForMember(dest => dest.Preference,
                    opt => opt.Ignore())
                .ForMember(dest => dest.PartnerManagerId,
                    opt => opt.Ignore())
                .ForMember(dest => dest.PartnerManager,
                    opt => opt.Ignore())
                .ForMember(dest => dest.Id,
                    opt => opt.Ignore());


            // Preferences
            CreateMap<Preference, PreferenceShortResponse>();
            CreateMap<Preference, PreferenceResponse>()
                .ForMember(dest => dest.Customers,
                    opt => opt.MapFrom(src => src.CustomerPreferences.Select(x => x.Customer)));
        }
    }
}